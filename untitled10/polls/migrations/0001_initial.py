# Generated by Django 2.1.1 on 2019-02-19 10:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Passwords',
            fields=[
                ('siteName', models.CharField(max_length=255)),
                ('password', models.CharField(max_length=255)),
                ('userName', models.CharField(max_length=255)),
                ('category', models.CharField(max_length=255)),
                ('id', models.AutoField(primary_key=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=200)),
                ('userPassword', models.CharField(max_length=255)),
                ('last_login', models.DateField(verbose_name='last_login')),
                ('email', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='passwords',
            name='user_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='polls.Users'),
        ),
    ]
