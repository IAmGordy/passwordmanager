from django.forms import modelform_factory
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect, render
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_GET
from django.views.generic import TemplateView
from rest_framework.decorators import api_view
from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView
from rest_framework.response import Response


from polls.models import Users, Passwords
from rest_framework import viewsets, status, request
from polls.api.serializers import UsersSerializer, PasswordsSerializer, PasswordsSerializer_user_id, \
    PasswordsSerializer_Category, UsersSerializer_email, UsersSerializer_password
from rest_framework import permissions
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    CreateAPIView,
    DestroyAPIView,
    UpdateAPIView
)
from polls.models import Passwords


class UsersViewSet(viewsets.ModelViewSet):
    queryset = Users.objects.all()
    serializer_class = UsersSerializer
    lookup_value_regex = '[^/]+'

class UsersViewSet_email(viewsets.ModelViewSet):
    serializer_class = UsersSerializer_email
    lookup_field = "email"
    lookup_value_regex = '[^/]+'
    queryset = Users.objects.all()
    """
    Mit dem Code darunter kann man nach gewähltem Parameter suchen
    """
    def get_queryset(self):
        queryset = Users.objects.all()
        username = self.request.query_params.get('email', None)
        if username is not None:
            queryset = queryset.filter(email=username)

        if 'email' in self.kwargs:
            return Users.objects.filter(email=self.kwargs['email'])
        else:
            return Users.objects.filter(email=self.kwargs['email'])
        return queryset

    def retrieve(self, request, *args, **kwargs):  # Change is here <<
        serializer = self.get_serializer(self.get_queryset(), many=True)
        return Response(data=serializer.data)


class UsersViewSet_password(viewsets.ModelViewSet):
    serializer_class = UsersSerializer_password
    lookup_field = "userPassword"
    queryset = Users.objects.all()
    """
    Mit dem Code darunter kann man nach gewähltem Parameter suchen
    """
    def get_queryset(self):
        queryset = Users.objects.all()
        username = self.request.query_params.get('userPassword', None)
        if username is not None:
            queryset = queryset.filter(userPassword=username)

        if 'userPassword' in self.kwargs:
            return Users.objects.filter(userPassword=self.kwargs['userPassword'])
        else:
            return Users.objects.filter(userPassword=self.kwargs['userPassword'])
        return queryset

    def retrieve(self, request, *args, **kwargs):  # Change is here <<
        serializer = self.get_serializer(self.get_queryset(), many=True)
        return Response(data=serializer.data)


class PasswordsViewSet(viewsets.ModelViewSet):
    queryset = Passwords.objects.all()
    serializer_class = PasswordsSerializer
    lookup_value_regex = '[^/]+'

class PasswordsViewSet_user_id(viewsets.ModelViewSet):
    serializer_class = PasswordsSerializer_user_id
    lookup_field = "user_id"
    queryset = Passwords.objects.all()
    """
    Mit dem Code darunter kann man nach gewähltem Parameter suchen
    """

    def get_queryset(self):
        queryset = Passwords.objects.all()
        username = self.request.query_params.get('user_id', None)
        if username is not None:
            queryset = queryset.filter(user_id=username)

        if 'user_id' in self.kwargs:
            return Passwords.objects.filter(user_id=self.kwargs['user_id'])
        else:
            return Passwords.objects.filter(user_id=self.kwargs['user_id'])

        return queryset

    def retrieve(self, request, *args, **kwargs):  # Change is here <<
        serializer = self.get_serializer(self.get_queryset(), many=True)
        return Response(data=serializer.data)

class PasswordsViewSet_Category(viewsets.ModelViewSet):
    serializer_class = PasswordsSerializer_Category
    lookup_field = "category"
    queryset = Passwords.objects.all()
    """
    Mit dem Code darunter kann man nach gewähltem Parameter suchen
    """

    def get_queryset(self):
        queryset = Passwords.objects.all()
        username = self.request.query_params.get('category', None)
        if username is not None:
            queryset = queryset.filter(category=username)

        if 'category' in self.kwargs:
            return Passwords.objects.filter(category=self.kwargs['category'])
        else:
            return Passwords.objects.filter(category=self.kwargs['category'])

        return queryset

    def retrieve(self, request, *args, **kwargs):  # Change is here <<
        serializer = self.get_serializer(self.get_queryset(), many=True)
        return Response(data=serializer.data)


class PasswordsListView(ListAPIView):
    queryset = Passwords.objects.all()
    serializer_class = PasswordsSerializer


class PasswordsDetailView(RetrieveAPIView):
    queryset = Passwords.objects.all()
    serializer_class = PasswordsSerializer


class PasswordsCreateView(CreateAPIView):
    queryset = Passwords.objects.all()
    serializer_class = PasswordsSerializer


class PasswordsUpdateView(UpdateAPIView):
    queryset = Passwords.objects.all()
    serializer_class = PasswordsSerializer


class PasswordsDeleteView(DestroyAPIView):
    queryset = Passwords.objects.all()
    serializer_class = PasswordsSerializer
