# from articles.api.views import ArticleViewSet
# from rest_framework.routers import DefaultRouter

# router = DefaultRouter()
# router.register(r'', ArticleViewSet, base_name='articles')
# urlpatterns = router.urls

from django.urls import path

from polls.api.views import (
    PasswordsListView,
    PasswordsDetailView,
    PasswordsCreateView,
    PasswordsUpdateView,
    PasswordsDeleteView
)

urlpatterns = [
    path('', PasswordsListView.as_view()),
    path('create/', PasswordsCreateView.as_view()),
    path('<pk>', PasswordsDetailView.as_view()),
    path('<pk>/update/', PasswordsUpdateView.as_view()),
    path('<pk>/delete/', PasswordsDeleteView.as_view())
]