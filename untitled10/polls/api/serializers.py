from polls.models import Users, Passwords
from rest_framework import serializers
from rest_framework.response import Response


class UsersSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Users
        fields = ('id', 'name', 'userPassword', 'last_login', 'email')
        many = True

class UsersSerializer_email(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Users
        fields = ('id', 'name', 'userPassword', 'last_login', 'email')
        many = True
        lookup_field = 'email'

class UsersSerializer_password(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Users
        fields = ('id', 'name', 'userPassword', 'last_login', 'email')
        many = True
        lookup_field = 'userPassword'

class PasswordsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Passwords
        fields = ('user_id', 'siteName', 'password', 'userName', 'category', 'id')
        many = True

class PasswordsSerializer_user_id(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Passwords
        fields = ('user_id', 'siteName', 'password', 'userName', 'category', 'id')
        many = True
        lookup_field = 'id'

class PasswordsSerializer_Category(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Passwords
        fields = ('user_id', 'siteName', 'password', 'userName', 'category', 'id')
        many = True
        lookup_field = 'category'