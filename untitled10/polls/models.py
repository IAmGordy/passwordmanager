from django.db import models


class Users(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200, null=False)
    userPassword = models.CharField(max_length=255, null=False)
    last_login = models.DateField('last_login')
    email = models.CharField(max_length=255, null=False)

class Passwords(models.Model):

    user_id = models.ForeignKey(Users, on_delete=models.CASCADE)
    siteName = models.CharField(max_length=255, null=False)
    password = models.CharField(max_length=255, null=False)
    userName = models.CharField(max_length=255, null=False)
    category = models.CharField(max_length=255)
    id = models.AutoField(primary_key=True)