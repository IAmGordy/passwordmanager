from django.conf.urls import url, include
from django.urls import path
from rest_framework import routers
from polls.api import views
from polls.api.views import (
    PasswordsListView,
    PasswordsDetailView,
    PasswordsCreateView,
    PasswordsUpdateView,
    PasswordsDeleteView
)

router = routers.DefaultRouter()
router.register(r'users', views.UsersViewSet)
router.register(r'users_email', views.UsersViewSet_email)
router.register(r'users_passwords', views.UsersViewSet_password)
router.register(r'passwords', views.PasswordsViewSet)
router.register(r'passwords_user_id', views.PasswordsViewSet_user_id)
router.register(r'passwords_category', views.PasswordsViewSet_Category)


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [


    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^', include(router.urls)),
    path('', PasswordsListView.as_view()),
    path('create/', PasswordsCreateView.as_view()),
    path('<pk>[^/.]+', PasswordsDetailView.as_view()),
    path('<pk>/update/', PasswordsUpdateView.as_view()),
    path('<pk>/delete/', PasswordsDeleteView.as_view()),
    url(r'^users/(?P<email>\w+)/(?P<userPassword>\w+)/$', views.UsersViewSet_email, name='Login'),
    url(r'^api/password_reset/', include('django_rest_passwordreset.urls', namespace='password_reset')),
]