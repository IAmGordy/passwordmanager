import React, { Component } from 'react';
import "./fixed.css"
import "./input.css"
import WebsiteData from "./WebsiteData";
import axios from "axios";
import SearchByCategory from './SearchByCategory';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';

const categories = [
    {
      value: 'Social Media'
      
    },
    {
      value: 'Bank'
      
    },
    {
      value: 'News'
    },
    {
      value: 'Websites'
    },
    {
        value: 'All Websites'
      }
   
  ];


class WebsiteList extends Component {
state ={
    websites: [],
    category:'',
   

}
handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

componentDidMount(){
    
   axios.get('http://localhost:8000/passwords/?user_id=7/')
   .then(res=> {
       this.setState({ 
           websites: res.data,
           category: 'All Websites'
       })
       console.log(res.data);
   })


}

searchByCategory= (event) =>{
    console.log((document.getElementById("category").value))
    let category = document.getElementById("category").value
    if(category === "All Websites"){
        axios.get('http://localhost:8000/passwords/')
   .then(res=> {
       this.setState({ 
           websites: res.data,
           
       })
       console.log(res.data);
   })
    }
    else{
    axios.get(`http://localhost:8000/passwords_category/${category}/`)
   .then(res=> {
       this.setState({ 
           websites: res.data,
           
       })
       console.log(res.data);
   })
}
}

click(event) {
    console.log(event.target.getAttribute('id'))
}

  render() {
    return (
    
     <div>
          <TextField
          id="category"
          select
          InputProps={{className: 'nohover'}}
          onChange={this.handleChange('category')}
          value={this.state.category}
          helperText="Search by Category"
          margin="dense"
        >
          {categories.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.value}
            </MenuItem>
          ))}
        </TextField>
        <Button variant="contained" color="primary" onClick={this.searchByCategory}>
              Search
        </Button>
        
      
        
        {this.state.websites.map(w=> <WebsiteData key={w.id} password_id={w.id} user_id={w.user_id} siteName={w.siteName} password={w.password} userName={w.userName} category={w.category}/>
         )}
         <hr className="hrborder"/>

     </div>
     
    );
  }
}

export default WebsiteList;
