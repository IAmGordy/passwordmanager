import React, { Component } from 'react';
import "./fixed.css"
import Button from '@material-ui/core/Button';
import axios from "axios";
import {Link} from 'react-router-dom';
import 'antd/dist/antd.css';  // or 'antd/dist/antd.less'
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Generate from './Generate';



import {
    Form, Input, message,
  } from 'antd';

  const info = () => {
    message.success('Data updated');
  };
  const error = () => {
    message.error('error has accured');
  };
  const passwordm = () => {
    message.error('Passwords do not match');
  };

  const categories = [
    {
      value: 'Social Media'
      
    },
    {
      value: 'Bank'
      
    },
    {
      value: 'News'
    },
    {
      value: 'Websites'
    }
   
  ];

class DetailedWebsite extends Component {
state ={
    website: {},
    userName:'',
    siteName:'',
    password:'',
    category:'',
    type: 'password',
    type2: 'password'

   
}
showHide = (e) => {
  e.preventDefault();
  e.stopPropagation();
 if(e.target.id === 'type' || e.target.id === 'typeh'){
  this.setState({
    type: this.state.type === 'input' ? 'password' : 'input'
  })  
}
  else{
  this.setState({
    type2: this.state.type2 === 'input' ? 'password' : 'input'
  })  
}
}

componentDidMount(){
    const password_id = this.props.match.params.password_id;
    console.log(password_id)
   axios.get(`http://localhost:8000/passwords/${password_id}/`)
   .then(res=> {
       this.setState({ 
           website: res.data,
           userName: res.data.userName,
           siteName: res.data.siteName,
           password: res.data.password,
           category: res.data.category
           
       })
       console.log(res.data);

   })
}
handleChange = name => event => {
  this.setState({
    [name]: event.target.value,
  });



  
}

handleFormSubmit = (event) =>{
    event.preventDefault();
    const password_id = this.props.match.params.password_id;
    let userName = event.target.elements.username.value;
    let siteName = event.target.elements.sitename.value;
    let password = event.target.elements.password.value;
    let passwordr = event.target.elements.passwordRepeat.value;
    let category = event.target.elements.category.value;
    if(password !== passwordr){
        passwordm();
    }
    else{

    if(userName==='')
    {
      userName=this.state.userName;
    }
    if(siteName==='')
    {
      siteName=this.state.siteName;
    }
    if(password==='')
    {
      password=this.state.password;
    }
    if(category==='')
    {
      category=this.state.category;
    }
  

    axios.put(`http://localhost:8000/passwords/${password_id}/`, {
            
            user_id:1,
            userName: userName,
            password: password,
            siteName: siteName,
            category: category

        })
        .then(res=> {
            console.log(1);
            info();

        })
        .catch(err=> {
            console.log(err);
            error();
        });
      }
}
  render() {
    return (
    
     <div>
       <Button variant="contained" color="primary" type="submit" href="/">
       <i class="fas fa-arrow-circle-left"></i>
          </Button>
        <p>Sitename: {this.state.userName}</p>
        <p>Username: {this.state.siteName}</p>
        <form noValidate autoComplete="off" onSubmit={this.handleFormSubmit}>
        
        <TextField
          id="username"
          label={this.state.website.userName}
         
          onChange={this.handleChange}
          InputProps={{className: 'nohover'}}
          helperText="Username"
          margin="normal"/>
         <TextField
          id="sitename"
          label={this.state.website.siteName}
          onChange={this.handleChange}
          helperText="Sitename"
          margin="normal" 
          InputProps={{className: 'nohover'}}/>
          
         
                 <TextField
          id="password"
          placeholder="Password*"
          required
          InputProps={{className: 'nohover'}}
          onChange={this.handleChange}
          type={this.state.type}
          autoComplete="current-password"
          margin="normal"
        />
        <span 
            
            className="password__show" 
            onClick={this.showHide}>
            {this.state.type === 'input' ? 
            <i id="type" class="far fa-lg fa-eye"></i> : 
            <i id="typeh" class="far fa-lg fa-eye-slash">
            </i>}
        </span>
        <TextField
          id="passwordRepeat"
          placeholder="Password Repeat*"
          required
          InputProps={{className: 'nohover'}}
          type={this.state.type2}
          autoComplete="current-password"
          margin="normal"
          onChange={this.handleChange}
        />
        <span 
            
            className="password__show" 
            onClick={this.showHide}>
            {this.state.type2 === 'input' ? 
            <i class="far fa-lg fa-eye"></i> : 
            <i class="far fa-lg fa-eye-slash">
            </i>}
        </span>
        <TextField
          id="category"
          select
          label="Select"
         
          InputProps={{className: 'nohover'}}
          onChange={this.handleChange('category')}
          value={this.state.category}
          helperText="Please select your currency"
          margin="normal"
        >
          {categories.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.value}
            </MenuItem>
          ))}
        </TextField>
          <Button variant="contained" color="primary" type="submit">
          Update Website
          </Button>
        </form>
        <Generate/>
      
     </div>
     
    );
  }
}

export default DetailedWebsite;
