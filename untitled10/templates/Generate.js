import React, { Component } from 'react';

import Button from '@material-ui/core/Button';
import { generate } from 'generate-password';
import Checkbox from '@material-ui/core/Checkbox';

import TextField from '@material-ui/core/TextField';

var generator = require('generate-password');

  class Generate extends Component {

    state = {
     
      uppercase: true,
      numbers: false,
      symbols: false,
      strict: false,
      excludech: false,
      length: 10
    };

    
    handleCheck = name => event => {
      this.setState({ [name]: event.target.checked });
    };
    
  
    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
    };

    generatePassword = (e) =>{

      
      document.getElementById('password').value='';
      document.getElementById('passwordRepeat').value='';

      var password = generator.generate({
        length: this.state.length,
        numbers: this.state.numbers,
        symbols: this.state.symbols,
        strict: this.state.strict,
        uppercase: this.state.uppercase,
        excludeSimilarCharacters: this.state.excludech
    });

    
    document.getElementById('password').value=password;
    document.getElementById('passwordRepeat').value=password
    
    }

    render() {
      
      return (
        <div>
   
   <Button variant="contained" color="primary" type="button" onClick={this.generatePassword}>
              Generate
             </Button>
            
          <TextField
          id="standard-number"
          label="Length"
          value={this.state.length}
          onChange={this.handleChange('length')}
          type="number"
         
          InputProps={{className: "nohover"}}
          inputProps={{min: "6", step: "1"}}
         
          InputLabelProps={{
            shrink: true,
          }}
         
        />
        
          <Checkbox
               checked={this.state.uppercase}
               onChange={this.handleCheck('uppercase')}
               value="uppercase"
               color="primary"
                />
              <span>Uppercase</span>
        <Checkbox
          checked={this.state.numbers}
          onChange={this.handleCheck('numbers')}
          value="numbers"
          color="primary"
        />
        <span>Numbers</span>
        <Checkbox
          checked={this.state.symbols}
          onChange={this.handleCheck('symbols')}
          value="symbols"
          color="primary"
        />
        <span>Symbols</span>
        <Checkbox
          checked={this.state.excludech}
          onChange={this.handleCheck('excludech')}
          value="excludech"
          color="primary"
        />
        <span>Exclude Similar Characters</span>
        <Checkbox
          checked={this.state.strict}
          onChange={this.handleCheck('strict')}
          value="strict"
          color="primary"
        />
        <span>Strict</span>
              
              
        </div>
      );
    }
  }

  export default Generate;
