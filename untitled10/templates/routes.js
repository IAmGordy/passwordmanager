import React from 'react';
import {Route} from 'react-router-dom';
import Main from './Main';
import DetailedWebsite from './DetailedWebsite';
import 'antd/dist/antd.css';  // or 'antd/dist/antd.less'
import AddWebsite from './AddWebsite';


const BaseRouter = () => (

    <div>
        <Route exact path='/' component={Main}/>
        <Route  path='/detailedview/:password_id' component={DetailedWebsite}/>
        <Route  path='/addWebsite' component={AddWebsite}/>
    </div>
);

export default BaseRouter;