import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import axios from "axios";




class SearchByCategory extends Component {

    state = {
       
        category: '',
        websites:[]
       
      };
  
      componentDidMount(){
        this.setState({
          category: "Social Media",
          
          
        })
      }
      searchByCategory = (event)=>{
        axios.get('http://localhost:8000/passwords/?category=Social Media/')
        .then(res=> {
            this.setState({ 
                websites: res.data
                
            })
            console.log(this.state.websites)
            
        })
      }

    
  render() {
    return (
    
      <div >
           <Button variant="contained" color="primary" onClick={this.searchByCategory}>
              Search
        </Button>
        
      
      
      </div>
     
    );
  }
}

export default SearchByCategory;
