import React, { Component } from 'react';
import "./fixed.css"
import WebsiteList from "./WebsiteList";
import {Link} from "react-router-dom";
import 'antd/dist/antd.css';  
import axios from "axios";
import Button from '@material-ui/core/Button';
import Export from "./Export";
import Import from "./Import";


class Main extends Component {

  state ={
    websites: [],
    
    
}  
  
componentDidMount(){
  axios.get('http://localhost:8000/passwords/?user_id=1/')
   .then(res=> {
       this.setState({ 
           websites: res.data
       })
       
   })
}

  render() {
    return (
    
      <div className="container top">
      <div className="row">
      <div className="col-8">
        <div className="row">
        <Button variant="contained" color="primary" href="/addWebsite">
        Add Website
      </Button>
            <Export websites={this.state.websites}/>
            <Import websites={this.state.websites}/>
            
        </div>
        <div className="row">
            
            <div className="container-fluid nopadding top">
            <WebsiteList/>
            </div>
        </div>
       </div>
        <div className="col-4">
      <p>Picture here</p>
        </div>
      </div>
      
      
      </div>
     
    );
  }
}

export default Main;
