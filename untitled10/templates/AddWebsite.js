import React, { Component } from 'react';
import "./fixed.css"
import axios from "axios";
import {Link} from 'react-router-dom';
import 'antd/dist/antd.css';  // or 'antd/dist/antd.less';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import './input.css';

import Generate from './Generate';



import {
    Form, Input, message,
  } from 'antd';

  
  var generator = require('generate-password');

  const categories = [
    {
      value: 'Social Media'
      
    },
    {
      value: 'Bank'
      
    },
    {
      value: 'News'
    },
    {
      value: 'Websites'
    }
   
  ];

  const info = () => {
    message.success('Data added');
  };
  const error = () => {
    message.error('Error has accured');
  };
  const passwordR = () => {
    message.error('Passwords are not equal');
  };

  class AddWebsite extends Component {

    state = {
      showPassword: false,
      category: '',
      uppercase: true,
      numbers: false,
      symbols: false,
      strict: false,
      excludech: false,
      length: 10,
      type: 'password',
      type2: 'password'
    
    };

    componentDidMount(){
      this.setState({
        category: "Social Media",
        showPassword: false,
        
      })
    }
    handleCheck = name => event => {
      this.setState({ [name]: event.target.checked });
    };
    
    handleFormSubmit = (event) =>{
        event.preventDefault();
        let username = event.target.elements.username.value;
        let sitename = event.target.elements.sitename.value;
        let password = event.target.elements.password.value;
        let category = event.target.elements.category.value;
        let passwordRepeat = event.target.elements.passwordRepeat.value;
        console.log(category)
        if(password !== passwordRepeat)
        {
            passwordR();
        }
        else{
        axios.post('http://localhost:8000/passwords/', {
            
            
            user_id: "1",
            siteName: sitename,
            password: password,
            userName: username,
            category: category

        })
        .then(res=> {
            console.log(1);
            info();
           


        })
        .catch(err=> {
            console.log(err);
            error();
        });
      }
        event.target.elements.username.value="";
        event.target.elements.sitename.value="";
        event.target.elements.password.value="";
        event.target.elements.passwordRepeat.value="";
        event.target.elements.category.value="Social Media";
    }
    handleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      });
    };

    generatePassword = (e) =>{

      document.getElementById('generatedPW').value='';
      document.getElementById('password').value='';
      document.getElementById('passwordRepeat').value='';

      var password = generator.generate({
        length: this.state.length,
        numbers: this.state.numbers,
        symbols: this.state.symbols,
        strict: this.state.strict,
        uppercase: this.state.uppercase,
        excludeSimilarCharacters: this.state.excludech
    });

    document.getElementById('generatedPW').value=password;
    document.getElementById('password').value=password;
    document.getElementById('passwordRepeat').value=password
    
    }
    showHide = (e) => {
      e.preventDefault();
      e.stopPropagation();
     if(e.target.id === 'type' || e.target.id === 'typeh'){
      this.setState({
        type: this.state.type === 'input' ? 'password' : 'input'
      })  
    }
      else{
      this.setState({
        type2: this.state.type2 === 'input' ? 'password' : 'input'
      })  
    }
    }

    render() {
      
      return (
        <div>
          <Button variant="contained" color="primary" href="/">
              Back
             </Button>
            <form onSubmit={this.handleFormSubmit} Validate>
            
            <TextField
                id="username"
                placeholder="Username*"
                required
                margin="dense"
                InputProps={{className: 'nohover'}}
               onChange={this.handleChange}
               helperText="Username"
              
               />
            <TextField
            required
               id="sitename"
               placeholder="Sitename*"
              onChange={this.handleChange}
              helperText="Sitename"
              InputProps={{className: 'nohover'}}
                 margin="dense"/>
                 <TextField
          id="password"
          placeholder="Password Repeat*"
          required
          InputProps={{className: 'nohover'}}
          onChange={this.handleChange}
          type={this.state.type}
          autoComplete="current-password"
          margin="dense"
        />
        <span 
            
            className="password__show" 
            onClick={this.showHide}>
            {this.state.type === 'input' ? 
            <i id="type" class="far fa-lg fa-eye"></i> : 
            <i id="typeh" class="far fa-lg fa-eye-slash">
            </i>}
        </span>
        <TextField
          id="passwordRepeat"
          placeholder="Password Repeat*"
          required
          InputProps={{className: 'nohover'}}
          type={this.state.type2}
          autoComplete="current-password"
          margin="dense"
          onChange={this.handleChange}
        />
        <span 
            
            className="password__show" 
            onClick={this.showHide}>
            {this.state.type2 === 'input' ? 
            <i class="far fa-lg fa-eye"></i> : 
            <i class="far fa-lg fa-eye-slash">
            </i>}
        </span>
        <TextField
          id="category"
          select
          label="Select"
         
          InputProps={{className: 'nohover'}}
          onChange={this.handleChange('category')}
          value={this.state.category}
          helperText="Please select your currency"
          margin="dense"
        >
          {categories.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.value}
            </MenuItem>
          ))}
        </TextField>
            
            <Button variant="contained" color="primary" type="submit">
              Update Website
             </Button>
             
            </form>
            <Generate/>

            
          
              
              
        </div>
      );
    }
  }

  export default AddWebsite;
