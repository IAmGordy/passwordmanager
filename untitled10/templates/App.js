import React, { Component } from 'react';
import "./fixed.css"
import Main from './Main';
import {BrowserRouter as Router} from 'react-router-dom';
import BaseRouter from './routes';

class App extends Component {
  render() {
    return (
      <div className="container-fluid">
      
      <Router>
        <BaseRouter/>
      </Router>
      </div>
    );
  }
}

export default App;
