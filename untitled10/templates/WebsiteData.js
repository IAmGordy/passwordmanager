import React, { Component } from 'react';
import "./fixed.css";
import axios from "axios";
import {Link} from 'react-router-dom';
import { Modal, message} from 'antd';
import Fab from '@material-ui/core/Fab';

const info = () => {
    message.success('Data deleted');
  };
  const canceled = () => {
    message.info('Deleting Data was canceled');
  };

axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.xsrfHeaderName = 'X-CSRFToken';

axios.defaults.withCredentials = true;
class WebsiteData extends Component {
    

    state = 
    {
      visible: false,
      deleteid: null
    }
    

    showModal = (event) => {

        const b = this.props.password_id;
        console.log(b);
        this.setState({
          visible: true,
          deleteid: b
        });
        

      }
    
      handleOk = (e) => {
          this.reply_click();
        console.log(e);
        this.setState({
          visible: false,
        });
        window.location.reload();
        info();
      }
    
      handleCancel = (e) => {
        console.log(e);
        this.setState({
          visible: false,
        });
        canceled();
      }

    
    reply_click = (event) => {
      const a = this.state.deleteid;
        console.log(a)
       

        axios.delete(`http://localhost:8000/passwords/${a}/`)
          .then(res => {
            console.log(res);
            console.log(res.data);
          })

          
    }
    

  render() {
    return (
    
     <div className="container">
        <div className="row">
        <div className="shadow-box">
          <div className="row">
            <div className="col-10">
              <div className="center-text">
            <span className="spanbold">Sitename:</span>
            <span className="list-v">{this.props.siteName}</span>
            <span className="spanbold">Username:</span>
            <span className="list-v">{this.props.userName}</span>
            <span className="spanbold">Category:</span>
            <span className="list-v">{this.props.category}</span>
                </div>
               </div>
            <div className="col-2">
              <div className="center-button">
            <Fab  href={`detailedview/${this.props.password_id}`} size="medium" color="primary" aria-label="Add"><i class="fas fa-pencil-alt fa-lg"></i></Fab>
            <Fab id={this.props.id} onClick={this.showModal} type="submit" size="medium" ><i class="fas fa-trash-alt fa-lg"></i></Fab>
            <Modal
              title="Delete Data"
              visible={this.state.visible}
              onOk={this.handleOk}
              onCancel={this.handleCancel}
            >
              <p>Do you really want to delete the data?</p>
              
             </Modal>
              </div>
             </div>
              </div>
             </div>
        </div>
     </div>
     
    );
  }
}

export default WebsiteData;
